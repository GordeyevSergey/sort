import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sort {
    private List<Integer> array;

    public static void main(String[] args) {
        String pathToFile = "Z:\\Test\\Input.txt";
        String outPathToFile = "Z:\\Test\\Output.txt";
        String inputLine = input(pathToFile);
        assert inputLine != null;
        String[] splitMassive = inputLine.split(" ");
        Integer[] inputArray = strToNum(splitMassive);
        Sort sort = new Sort(inputArray);
        sort.compute();
        output(sort.getResult(), outPathToFile);
    }

    private static String input(String pathToFile) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))) {
            return bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Integer[] strToNum(String[] splitString) {
        Integer[] result = new Integer[splitString.length];
        for (int i = 0; i < splitString.length; i++) {
            result[i] = Integer.parseInt(splitString[i]);
        }
        return result;
    }

    private static void output(List<Integer> result, String outPathToFile) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(outPathToFile))) {
            try {
                output.write(result.toString() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Sort(Integer[] array) {
        this.array = Arrays.asList(array);
    }

    private void compute() {
        array.sort((lhs, rhs) -> {
            int compareFreq = Collections.frequency(array, rhs) - Collections.frequency(array, lhs);
            return compareFreq != 0 ?
                    compareFreq :
                    rhs - lhs;
        });
    }

    private List<Integer> getResult() {
        return array;
    }
}